<?php
require_once 'Api.php';
require(ROOT . '/Components/Db.php');
require(ROOT . '/Models/Book.php');

class BooksApi extends Api
{
    public $apiName = 'books';

	private $allowedKeys = array('name', 'author', 'pages', 'price', 'per_page',
 	'year', 'theme', 'isbn', 'pages_count_max', 'pages_count_min', 'page', 'sortby',
	'order');

    public function indexAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
        $books = Book::getAllBooks();
        if($books){
            return $this->response($books, 200);
        }
        return $this->response($this->responseGenerator('Books not found', 404), 404);
    }

    public function viewAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
        $data = array();
		foreach ($this->requestParams as $key => $value) {
			$data[strtolower($key)] = strtolower($value);
			if (!in_array(strtolower($key),$this->allowedKeys))
				return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
		}
        if (!$response = $this->validationUpdate($data, true)) {
            if (isset($data['page']) && $data['page'] == 0)
				return $this->response(
					$this->responseGenerator('you should start from first page', 404), 404);
            if($book = Book::search($data)) {
                return $this->response($book, 200);
            } else {
				return $this->response($this->responseGenerator('Book not found', 404), 404);
            }
        } else {
            return $this->response($this->responseGenerator($response, 400), 400);
        }
    }

    public function createAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
    		$data = array();
			foreach ($this->requestParams as $key => $value) {
				$data[strtolower($key)] = $value;
				if (!in_array(strtolower($key),$this->allowedKeys))
					return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
			}
    		if (!$response = $this->validationCreate($data)) {
    		    if (Book::create($data)) {
    		        return $this->response(
						$this->responseGenerator("Book created", 200), 200);
    	      } else {
    				return $this->response(
						$this->responseGenerator("Smthng Wrong", 500), 500);
    			  }
    		} else {
    			  return $this->response($this->responseGenerator($response, 400), 400);
    		}
    }

    public function updateAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
        $data = array();
		foreach ($this->requestParams as $key => $value) {
			$data[strtolower($key)] = $value;
			if (!in_array(strtolower($key),$this->allowedKeys))
				return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
		}
        if (!$response = $this->validationUpdate($data)) {
            if (isset($data['name']) && Book::nameExists($data['name'])) {
                if (Book::update($data)) {
                    return $this->response(
						$this->responseGenerator("Book ".$data['name']." updated!", 200), 200);
                } else {
					return $this->response(
						$this->responseGenerator("Smthng Wrong", 500), 500);
                }
            } else {
                return $this->response(
					$this->responseGenerator('please enter valid book name', 500), 500);
            }
        } else {
            return $this->response($this->responseGenerator($response, 400), 400);
        }
    }

    public function deleteAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
        $data = array();
		foreach ($this->requestParams as $key => $value) {
			$data[strtolower($key)] = $value;
			if (!in_array(strtolower($key),$this->allowedKeys))
				return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
		}
        if (isset($data['name']) && Book::nameExists($data['name'])) {
            if (Book::delete($data)) {
				return $this->response(
                	$this->responseGenerator("Book ".$data['name']." deleted!", 200), 200);
            } else {
                return $this->response(
					$this->responseGenerator("Smthng Wrong", 500), 500);
            }
        } else {
            return $this->response(
				$this->responseGenerator('please enter valid book name', 500), 500);
        }
    }

    private function validationCreate($data)
    {
        if ($data['pages'] <= 0 || is_int($data['pages']) || !strlen($data['pages']))
    			  return "invalid value in field 'pages'";
    		if ($data['price'] <= 0 || is_int($data['price']) || !strlen($data['price']))
    			 return "invalid value in field 'price'";
    		if ($data['year'] <= 0 || is_int($data['year']) || !strlen($data['year']))
    			 return "invalid value in field 'year'";
    		if (!strlen($data['author']))
    			 return "invalid value in field 'author'";
    		if (!strlen($data['name']))
    			 return "invalid value in field 'name'";
    		if (!strlen($data['theme']))
    			 return "invalid value in field 'theme'";
    		if ($data['isbn'] <= 0 || is_int($data['isbn']) || !strlen($data['isbn']))
    			 return "invalid value in field 'ISBN'";
    		if (Book::isbnExists($data['isbn']))
    			 return "This ISBN already exists";
    		return false;
    }

    private function validationUpdate($data, $get = false)
    {
        if (isset($data['pages_count_max'])) {
    		if ($data['pages_count_max'] <= 0 || is_int($data['pages_count_max'])
    			 || !strlen($data['pages_count_max']) ||
    			 	$data['pages_count_max'] < $data['pages_count_min'])
    				return "invalid value in field 'pages_count_max'";
    		}
		if (isset($data['pages_count_min'])) {
			if ($data['pages_count_min'] <= 0 || is_int($data['pages_count_min'])
			 || !strlen($data['pages_count_min']) ||
			 	$data['pages_count_max'] < $data['pages_count_min'])
				return "invalid value in field 'pages_count_min'";
		}
		if (isset($data['page'])) {
			if ($data['page'] <= 0 || is_int($data['page']) || !strlen($data['page']))
				return "invalid value in field 'page'";
		}
		if (isset($data['pages'])) {
			if ($data['pages'] <= 0 || is_int($data['pages']) || !strlen($data['pages']))
				return "invalid value in field 'pages'";
		}
		if (isset($data['price'])) {
			if ($data['price'] <= 0 || is_int($data['price']) || !strlen($data['price']))
				return "invalid value in field 'price'";
		}
		if (isset($data['year'])) {
			if ($data['year'] <= 0 || is_int($data['year']) || !strlen($data['year']))
				return "invalid value in field 'year'";
		}
		if (isset($data['author'])) {
			if (!strlen($data['author']))
				return "invalid value in field 'author'";
		}
		if (isset($data['name'])) {
			if (!strlen($data['name']))
				return "invalid value in field 'name'";
		}
		if (isset($data['theme'])) {
			if (!strlen($data['theme']))
				return "invalid value in field 'theme'";
		}
		if (isset($data['isbn']) && $get = false) {
			if ($data['isbn'] <= 0 || is_int($data['isbn']) || !strlen($data['isbn']))
				return "invalid value in field 'ISBN'";
			if (Book::isbnExists($data['isbn']))
				return "This ISBN already exists";
		}
		return false;
    }

  public function listener()
  {
      $ip = $_SERVER['REMOTE_ADDR'];
      $db = Db::getConnection();
      $sql = "INSERT INTO all_visits (ip,date) VALUES
           (INET_ATON('".$ip."'),'".time()."')";
      $insert = $db->prepare($sql);
      $insert->execute();
      $sql = "SELECT count(id) FROM all_visits WHERE
           (ip=INET_ATON('".$ip."') and date>'".(time()-1)."') LIMIT 1";
      $check = $db->prepare($sql);
      $check->execute();
      $result = $check->fetch(PDO::FETCH_ASSOC);


      if ($result['count(id)'] >= 100) {
          $sql = "INSERT INTO black_list_ip (ip,date) VALUES
                  (INET_ATON('".$ip."'),'".time()."')";
          $insert = $db->prepare($sql);
          $insert->execute();
          return "Too many requests";
      } else {
      	return false;
      }
  }

}
