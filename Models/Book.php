<?php


require_once(ROOT . '/Components/Db.php');

use PDO;

class Book
{
	public function getBookByName($name)
	{
		$db = Db::getConnection();
		$sql = "SELECT * FROM books
	 			WHERE name=:name";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->execute();
		$book = $result->fetch(PDO::FETCH_ASSOC);
		return $book;
	}

	public static function getAllBooks()
	{
		$db = Db::getConnection();
		$sql_c = "SELECT COUNT(*) FROM books ";
		$count = $db->prepare($sql_c);
		$count->execute();
		$pages = $count->fetch(PDO::FETCH_NUM);
		$sql = "SELECT * FROM books";
		$result =$db->prepare($sql);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $books[$i] = $row;
			$i++;
		}
		if (isset($data['per_page']) && isset($data['page'])) {
			$per_page = (int)$data['per_page'];
			$totalPages = $pages[0];
			$tmp = array(
				'page' => $data['page'],
				'total_pages' => ceil($totalPages / $per_page),
				'items_per_page' => $data['per_page']
			);
			array_unshift($books, $tmp);
		} elseif (isset($data['page'])) {
			$per_page = 10;
			var_dump($data['page']);
			$totalPages = $pages[0];
			$tmp = array(
				'page' => $data['page'],
				'total_pages' => ceil($totalPages / $per_page),
				'items_per_page' => $per_page
			);
			array_unshift($books, $tmp);
		} else {
			if (isset($data['per_page']))
				$per_page = (int)$data['per_page'];
			else
				$per_page = 10;
			$totalPages = $pages[0];
			$tmp = array(
				'page' => 1,
				'total_pages' => ceil($totalPages / $per_page),
				'items_per_page' => $per_page
			);
			array_unshift($books, $tmp);
		}
		return $books;
	}

	public static function search($data)
	{
		// var_dump($data);
		$db = Db::getConnection();
		$count = true;
		$sql_c = "SELECT COUNT(*) FROM books " . self::formatView($data, $count);
		$count = $db->prepare($sql_c);
		$count->execute();
		$pages = $count->fetch(PDO::FETCH_NUM);
		$sql = "SELECT * FROM books " . self::formatView($data);
		$result = $db->prepare($sql);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $books[$i] = $row;
			$i++;
		}
		if (isset($data['per_page']) && isset($data['page'])) {
			$per_page = (int)$data['per_page'];
			$totalPages = $pages[0];
			$tmp = array(
				'page' => $data['page'],
				'total_pages' => ceil($totalPages / $per_page),
				'items_per_page' => $data['per_page']
			);
			array_unshift($books, $tmp);
		} else {
			if (isset($data['per_page']))
				$per_page = (int)$data['per_page'];
			else
				$per_page = 10;
			$totalPages = $pages[0];
			$tmp = array(
				'page' => 1,
				'total_pages' => ceil($totalPages / $per_page),
				'items_per_page' => $per_page
			);
			array_unshift($books, $tmp);
		}

		return $books;
	}

	public static function create($data)
	{
		$db = Db::getConnection();
		$sql = 'INSERT INTO books
				(name, author, theme, pages, year, price, isbn)' .
		 		'VALUES(:name, :author, :theme, :pages, :year, :price,:isbn)';
		$result = $db->prepare($sql);
		$result->bindParam(':name', $data['name'], PDO::PARAM_STR);
		$result->bindParam(':author', $data['author'], PDO::PARAM_STR);
		$result->bindParam(':theme', $data['theme'], PDO::PARAM_STR);
		$result->bindParam(':pages', $data['pages'], PDO::PARAM_INT);
		$result->bindParam(':year', $data['year'], PDO::PARAM_INT);
		$result->bindParam(':price', $data['price'], PDO::PARAM_INT);
		$result->bindParam(':isbn', $data['isbn'], PDO::PARAM_INT);
		return $result->execute();
	}

	public static function update($data)
	{
		$db = Db::getConnection();
		$sql = 'UPDATE books SET ' . self::formatUpdate($data) . "WHERE name=:name";
		// var_dump($sql);
		$result = $db->prepare($sql);
		$result->bindParam(':name', $data['name'], PDO::PARAM_STR);
		return $result->execute();
	}

	public static function delete($data)
	{
		$db = Db::getConnection();
		$sql = "DELETE FROM books WHERE name=:name";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $data['name'], PDO::PARAM_STR);
		return $result->execute();
	}

	public static function formatUpdate($sql)
	{
		$i = 0;
		$str = '';
		$len = count($sql) - 1;
		foreach ($sql as $key => $value) {
			if ($len == $i) {
				$str .=  $key . '=' . "'" . $value. "' ";
			} else {
				$str .=  $key . '=' . "'" . $value. "', ";
			}
			$i++;
		}
		return $str;
	}

	public static function formatView($sql, $count = false)
	{
			$i = 0;
			$str = '';
			// handling keys
			foreach ($sql as $key => $value) {
				if ($sql['page'] === $value ||
					$sql['pages_count_min'] === $value ||
					$sql['pages_count_max'] === $value ||
					$sql['per_page'] === $value ||
					$sql['sortBy'] === $value ||
					$sql['order'] === $value)
					continue ;
				if ($i < 1)
					$str .= 'WHERE ' . $key . '=' . "'" . $value. "' ";
				if ($i >= 1)
					$str .= 'AND ' . $key . '=' . "'" . $value. "' ";

				$i++;
			}
			// handling pages count
			if ($sql['pages_count_min'] && $sql['pages_count_max']) {
				if (!substr($str, "WHERE")) {
					$str .= " AND pages BETWEEN " . $sql['pages_count_min'].
					" AND " . $sql['pages_count_max'] ;
				} else {
					$str .= "WHERE pages BETWEEN " . $sql['pages_count_min'].
					" AND " . $sql['pages_count_max'] ;
				}
			} elseif (isset($sql['pages_count_min'])) {
				if (!substr($str, "WHERE")) {
					$str .= " AND pages >= " . $sql['pages_count_min'];
				} else {
					$str .= " WHERE pages >= " . $sql['pages_count_min'];
				}
			} else if (isset($sql['pages_count_max'])) {
				if (!substr($str, "WHERE")) {
					$str .= " AND pages <= " . $sql['pages_count_max'];
				} else {
					$str .= " WHERE pages <= " . $sql['pages_count_max'];
				};
			}
			//checking flag 'count', just for couning total_pages
			if ($count == true) {
				return $str;
			} else {
				//sort handling
				if (isset($sql['sortBy']) && isset($sql['order'])) {
					$str .= " ORDER BY " . $sql['sortBy'] . " ".  $sql['order'];
				}
				// pagination handling
				if(isset($sql['per_page']) && isset($sql['page'])) {
					$str .= " LIMIT " . $sql['per_page'] . " OFFSET " .
						($sql['per_page'] * ($sql['page'] - 1))  ;
				} elseif(isset($sql['per_page'])) {
					$str .= " LIMIT " . $sql['per_page'] . " OFFSET " .
						($sql['per_page'] * (1 - 1))  ;
				} else {
					$str .= "LIMIT 10";
				}
			}

			return $str;
	}

	public function isbnExists($isbn)
	{
		$db = Db::getConnection();
		$sql = "SELECT COUNT(*) FROM books WHERE isbn=:isbn";
		$result = $db->prepare($sql);
		$result->bindParam(':isbn', $isbn, PDO::PARAM_INT);
		$result->execute();
		$res = $result->fetch();
		return ($res[0]);
	}

	public function nameExists($name)
	{
		$db = Db::getConnection();
		$sql = "SELECT COUNT(*) FROM books WHERE name=:name";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->execute();
		$res = $result->fetch();
		return ($res[0]);
	}
}
