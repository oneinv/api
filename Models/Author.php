<?php

require_once(ROOT . '/Components/Db.php');

use PDO;

class Author
{
	public function getByName($name)
	{
		$db = Db::getConnection();
		$sql = "SELECT a.author_name, a.b_day,
				GROUP_CONCAT(t.theme_name) AS 'Themes'
		 		FROM authors a
				JOIN links l ON l.author_id = a.author_id
				JOIN themes t ON t.theme_id = l.theme_id
				WHERE a.author_name=:name GROUP BY a.author_id";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->execute();
		$themes = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $themes = array_merge($themes, explode(',', $row['Themes']));
		}
		if ($themes)
			return array(
				"author" => $name,
				"themes" => $themes
			);
		else
			return array(
				"author" => $name,
				"error" => array(
					"error_msg" => "No such author.",
					"error_code" => 400
				)
			);
	}

	public function getByTheme($theme)
	{
		$db = Db::getConnection();
		$sql = "SELECT t.theme_name,
				GROUP_CONCAT(a.author_name) AS 'Authors'
		 		FROM themes t
				JOIN links l ON t.theme_id = l.theme_id
				JOIN authors a ON l.author_id = a.author_id
				WHERE t.theme_name=:theme GROUP BY t.theme_id";
		$result = $db->prepare($sql);
		$result->bindParam(':theme', $theme, PDO::PARAM_STR);
		$result->execute();
		$authors = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$authors = array_merge($authors, explode(',', $row['Authors']));
		}
		if ($authors)
			return array(
				"theme" => $theme,
				"authors" => $authors
			);
		else
			return array(
				"theme" => $theme,
				"error" => array(
					"error_msg" => "No such author.",
					 "error_code" => 400
				 )
		 	);
	}

	public function getByThemeAndName($theme, $name)
	{
		$db = Db::getConnection();
		$sql = "SELECT t.theme_name,
				GROUP_CONCAT(a.author_name) AS 'Authors'
		 		FROM themes t
				JOIN links l ON t.theme_id = l.theme_id
				JOIN authors a ON l.author_id = a.author_id
				WHERE t.theme_name=:theme AND a.author_name=:name GROUP BY t.theme_id";
		$result = $db->prepare($sql);
		$result->bindParam(':theme', $theme, PDO::PARAM_STR);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->execute();
		$authors = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$authors = array_merge($authors, explode(',', $row['Authors']));
		}
		if ($authors)
			return array(
				"theme" => $theme,
				"author" => $name,
				"authors" => $authors
			);
		else
			return array(
				"theme" => $theme,
				"author" => $name,
				"error" => array(
					"error_msg" => "No such author.",
					 "error_code" => 400
				 )
		 	);
	}

	public static function getAllAuthors()
	{
		$db = Db::getConnection();
		$sql = "SELECT a.author_name, a.b_day,
				GROUP_CONCAT(t.theme_name) AS 'Themes'
		 		FROM authors a
				JOIN links l ON l.author_id = a.author_id
				JOIN themes t ON t.theme_id = l.theme_id
				GROUP BY a.author_id";
		$result =$db->prepare($sql);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $authors[$i] = $row;
			$i++;
		}
		return $authors;
	}
}
