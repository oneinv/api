<?php

define('ROOT', dirname(__DIR__, 1) . '/api');

if (strrpos($_SERVER['REDIRECT_URL'], '/api/books') === 0) {
    require_once 'BooksApi.php';
    $apiClass = new booksApi();
}
elseif (strrpos($_SERVER['REDIRECT_URL'], '/api/authors') === 0) {
    require_once 'AuthorsApi.php';
    $apiClass = new authorsApi();
}

if ($apiClass) {
    try {
        $api = new $apiClass();
        echo $api->run();
    } catch (Exception $e) {
        echo json_encode(Array('error' => $e->getMessage()));
    }
}
else
    echo "Enter correct api name, smth like 'authors' or 'books' ";
