# api

This is documentation for small RestApi with books and authors.

Api contains following methods:
1. **books**
2. **authors**

Books can may take the following keys:

for finding books:

   - 'name' : string,
   - 'author' : string,
   - 'pages' : integer,
   - 'price' : integer,
   - 'year' : integer,
   - 'isbn' : integer UNIQ,
   - 'theme' : string

for pagination and sorting:
- 'per_page' : integer,
- 'pages_count_max' : integer,
- 'pages_count_min' : integer,
- 'page' : integer,
- 'sortby' : string,
- 'order' : string(ASC/DESC)

1. **GET**

*example:*

	curl -X GET 'http://localhost:8100/api/books/?author=King&name=Test&pages=100'

*response:*

	[
		{
			"page" : 1,
			"total_pages" : 1,
			"items_per_page" : 10
		},
		{
			"id" : "19",
			"author" : "King",
			"name" : "Test",
			"theme" : "Fantasy",
			"pages" : "100",
			"year" : "1987",
			"price" : "120",
			"isbn" : "12346"
		}
	]

2. **POST**

*example:*

	curl -X POST 'http://localhost:8100/api/books/?author=King&name=Test&pages=100&price=120&year=1987&theme=Fantasy&isbn=12346'

*response:*

	{
		"repsone" : {
			"message" : "Book created",
			"code" : 200
		}
	}

3. **PUT**

*example:*

	curl -X PUT 'http://localhost:8100/api/books/?author=King&name=Test&pages=100&price=111&year=1987&theme=Fantasy&isbn=12346'

*response:*

	{
		"repsone" : {
			"message" : "Book Test updated!",
			"code" : 200
		}
	}

4. **DELETE**

*example:*

	curl -X DELETE 'http://localhost:8100/api/books/?author=King&name=Test&pages=100&price=111&year=1987&theme=KEK&isbn=12346'

*response:*

	{
		"repsone" : {
			"message" : "Book Test deleted!",
			"code" : 200
		}
	}

Authors can may take the following keys:
for finding books:

- 'name' : string,
- 'theme' : string,

1. **GET**

*example:*

	curl -X GET 'http://localhost:8100/api/authors/?name=King'

*response:*

	{
		"author" : "king",
		"themes" : [
			"fantasy",
			"horror",
			"thrill"
		]
	}

If something go wrong api returns json with error explanation:

		{
			"repsone":{
				"message" : "This ISBN already exists",
				"code" : 400
			}
		}
