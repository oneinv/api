<?php
require_once 'Api.php';
require(ROOT . '/Models/Author.php');

class AuthorsApi extends Api
{
    public $apiName = 'authors';

	private $allowedKeys = array('name', 'theme');

    public function indexAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
        if($authors = Author::getAllAuthors()){
            return $this->response($authors, 200);
        }
        return $this->response(
			$this->responseGenerator('No auhtors for you ;(', 404), 404);
    }

    public function viewAction()
    {
        if ($response = $this->listener())
            return $this->response($response, 429);
	    $data = array();
		foreach ($this->requestParams as $key => $value) {
			$data[strtolower($key)] = strtolower($value);
			if (!in_array(strtolower($key),$this->allowedKeys))
				return $this->response($this->responseGenerator('Unknown parametr', 404), 404);
		}
	    if (!$response = $this->validationUpdate($data)) {
		   if (isset($data['page']) && $data['page'] == 0)
		       return $this->response(
				   $this->responseGenerator("You should start from the first page" , 404), 404);
           if (isset($data['theme']) && isset($data['name'])) {
               $author = Author::getByThemeAndName($data['theme'], $data['name']);
               return $this->response($author, 200);
           }
           if (isset($data['theme']) && ($author = Author::getByTheme($data['theme']))) {
               return $this->response($author, 200);
           } elseif (isset($data['name']) && ($author = Author::getByName($data['name']))) {
               return $this->response($author, 200);
    	   } else {
			   return $this->response(
				   $this->responseGenerator('Author ' . $data['name'] ." not found" , 404), 404);
		   }
	    } else {
		    return $this->response($response, 500);
	    }
    }

    public function createAction()
    {
        //
    }

    public function updateAction()
    {
        //
    }

    public function deleteAction()
    {
        //
    }

	private function validationUpdate($data, $get = false)
    {
        if (isset($data['pages_count_max'])) {
    		if ($data['pages_count_max'] <= 0 || is_int($data['pages_count_max'])
    			 || !strlen($data['pages_count_max']) ||
    			 	$data['pages_count_max'] < $data['pages_count_min'])
    				return "invalid value in field 'pages_count_max'";
    		}
		if (isset($data['pages_count_min'])) {
			if ($data['pages_count_min'] <= 0 || is_int($data['pages_count_min'])
			 || !strlen($data['pages_count_min']) ||
			 	$data['pages_count_max'] < $data['pages_count_min'])
				return "invalid value in field 'pages_count_min'";
		}
		if (isset($data['page'])) {
			if ($data['page'] <= 0 || is_int($data['page']) || !strlen($data['page']))
				return "invalid value in field 'page'";
		}
		if (isset($data['pages'])) {
			if ($data['pages'] <= 0 || is_int($data['pages']) || !strlen($data['pages']))
				return "invalid value in field 'pages'";
		}
		if (isset($data['price'])) {
			if ($data['price'] <= 0 || is_int($data['price']) || !strlen($data['price']))
				return "invalid value in field 'price'";
		}
		if (isset($data['year'])) {
			if ($data['year'] <= 0 || is_int($data['year']) || !strlen($data['year']))
				return "invalid value in field 'year'";
		}
		if (isset($data['author'])) {
			if (!strlen($data['author']))
				return "invalid value in field 'author'";
		}
		if (isset($data['name'])) {
			if (!strlen($data['name']))
				return "invalid value in field 'name'";
		}
		if (isset($data['theme'])) {
			if (!strlen($data['theme']))
				return "invalid value in field 'theme'";
		}
		if (isset($data['isbn']) && $get = false) {
			if ($data['isbn'] <= 0 || is_int($data['isbn']) || !strlen($data['isbn']))
				return "invalid value in field 'ISBN'";
			if (Book::isbnExists($data['isbn']))
				return "This ISBN already exists";
		}
		return false;
    }
  	public function listener()
  	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$db = Db::getConnection();
		$sql = "INSERT INTO all_visits (ip,date) VALUES
		   (INET_ATON('".$ip."'),'".time()."')";
		$insert = $db->prepare($sql);
		$insert->execute();
		$sql = "SELECT count(id) FROM all_visits WHERE
		   (ip=INET_ATON('".$ip."') and date>'".(time()-1)."') LIMIT 1";
		$check = $db->prepare($sql);
		$check->execute();
		$result = $check->fetch(PDO::FETCH_ASSOC);


		if ($result['count(id)'] >= 100) {
		  $sql = "INSERT INTO black_list_ip (ip,date) VALUES
		          (INET_ATON('".$ip."'),'".time()."')";
		  $insert = $db->prepare($sql);
		  $insert->execute();
		  return "Too many requests";
		} else {
		  return false;
		}
  	}

}
